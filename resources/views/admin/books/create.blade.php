@extends('admin.layouts.master')

@section('content')
    <div id="content-wrapper">
        <div class="container col-md-6">
            <form action={{ route('admin.books.store') }} method="POST" enctype="multipart/form-data">
                @method('post')
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                    @error('name')
                    <div class="has-feedback text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="type">Type:</label>
                    <input type="text" class="form-control" id="type" name="type" value="{{ old('type') }}" required>
                    @error('type')
                    <div class="has-feedback text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="des">Description:</label>
                    <input type="text" class="form-control" id="des" name="des" value="{{ old('des') }}" required>
                    @error('des')
                    <div class="has-feedback text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group" >
                    <label for="url">File pdf:</label>
                    <input type="file" name="url" multiple="multiple" required>
                    @error('url')
                    <div class="has-feedback text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
