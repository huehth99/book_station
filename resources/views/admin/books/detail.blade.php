@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-6" style="background: #4dc0b5">
                <iframe src="{{ asset('storage/pdf/'.$book->url) }}" title="{{ $book->name }}" width="100%" height="600"></iframe>
            </div>
            <div class="col-6 bg-info">
                <br>
                <h4 style="color: #0D3349">Id: {{ $book->id }}</h4>
                <h4 style="color: #0D3349">Name: {{ $book->name }}</h4>
                <h4 style="color: #0D3349">Type: {{ $book->type }}</h4>
                <h4 style="color: #0D3349">Description: {{ $book->des }}</h4>
                <h4 style="color: #0D3349">Number of page: {{ $book->page }}</h4>
                <h4 style="color: #0D3349">Views: {{ $book->views }}</h4>
                <h4 style="color: #0D3349">Admin created: {{ $created_by }} in {{ $book->created_at }}</h4>
                <h4 style="color: #0D3349">Admin updated: {{ $updated_by }} in {{ $book->updated_at }}</h4>

            </div>
        </div>
    </div>
@endsection
