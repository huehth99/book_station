@extends('admin.layouts.master')

@section('content')
    <div>
        @if($errors->any())
            <h4 style="color: red">{{$errors->first()}}</h4>
        @endif
        <form method='post' action='' enctype='multipart/form-data'>
            @csrf
            <label for="url">File upload (.zip):</label>
            <input type='file' name='file'><br/>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
