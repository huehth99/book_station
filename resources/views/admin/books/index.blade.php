@extends('admin.layouts.master')

@section('content')
    <div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
                <th scope="col">Url</th>
                <th scope="col">Page</th>
                <th scope="col">Views</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($books as $book)
                <tr>
                    <th scope="row">{{ $book->id }}</th>
                    <td>
                        <a href="{{ route('admin.books.show', $book->id)  }}">
                            {{ $book->name }}
                        </a></td>
                    <td>{{ $book->type }}</td>
                    <td>{{ $book->des }}</td>
                    <td>
                        <a href="{{ asset('storage/pdf/'.$book->url) }}">{{ $book->url }}</a>
                    </td>
                    <td>{{ $book->page }}</td>
                    <td>{{ $book->views }}</td>
                    <td>
                        <button type="button" class="btn btn-primary">
                            <a href="{{ route('admin.books.edit', $book->id) }}" style="color: white">Edit</a>
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-danger delete-book" data-id="{{ $book->id }}" type="button">
                            Delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('js')
<script src="{{ asset('js/deletebook.js') }}"></script>
@endpush
