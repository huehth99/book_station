<br>
<div class="btn-group dropright" style="display: list-item">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Manage Book
    </button>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="{{ route('admin.books.index') }}">List books</a>
        <a class="dropdown-item" href="{{ route('admin.books.create') }}">Add book</a>
        <a class="dropdown-item" href="{{ route('admin.books.addlist') }}">Add list book</a>
    </div>
</div>
