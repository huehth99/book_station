<!-- Header -->
<header>
{{--    <nav class="navbar navbar-light bg-white shadow-sm">--}}
    <div class="container-fluid shadow-sm">
        <div class="row">
            <div class="col-2 bg-light">
                <a href="{{ route('home.index') }}" title="The Book Station: Free for everyone">
                    <div>
                        <img src="{{ asset('storage/images/logo.png') }}">
                    </div>
                </a>
            </div>
            <div class="col-8 bg-info">
                <h2 style="color: whitesmoke; text-align: center">
                    Welcome to Book Station!
                </h2>
            </div>
            <div class="col-2 bg-light">
                <nav class="navbar navbar-expand-sm">
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="btn btn-info" role="button" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </nav>
            </div>
        </div>
    </div>
{{--    </nav>--}}
</header>

