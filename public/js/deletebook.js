$(document).ready(function() {
    $('.delete-book').click(function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        var $tr = $(this).closest('tr');
        swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: 'books/' + id,
                        type: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: id,
                        },
                        success: function () {
                            $tr.find('td').fadeOut(500, function () {
                                $tr.remove();
                            });
                            swal("Deleted!", "The book has been successfully deleted!", "success");
                        }
                    });
                } else {
                    swal({
                        title: "Canceled",
                        type: "error",
                    });
                }
            });
    });
});
