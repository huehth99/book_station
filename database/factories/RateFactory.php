<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rate;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    $ratables = [
        \App\Models\Book::class,
        \App\Models\Review::class,
    ];
    $ratable_type = $faker->randomElement($ratables);
    $ratable = factory($ratable_type)->create();
    $user_id = \App\Models\User::all()->random()->id;
    return [
        'ratable_id' => $ratable->id,
        'ratable_type' => $ratable_type,
        'user_id' => $user_id,
        'created_by' => $user_id,
        'updated_by' => $user_id,
        'star' => $faker->numberBetween(0, 5),
    ];
});
