<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Follow;
use Faker\Generator as Faker;

$factory->define(Follow::class, function (Faker $faker) {
    $user_id = \App\Models\User::all()->random()->id;
    $follower_id = \App\Models\User::all()->random()->id;
    return array(
        'user_id' => $user_id,
        'follower_id' => ($follower_id == $user_id) ? $user_id + 1 : $follower_id,
        'created_by' => $user_id,
        'updated_by' => $user_id,
    );
});
