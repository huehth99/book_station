<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    $commentables = [
        \App\Models\Book::class,
        \App\Models\Review::class,
    ];
    $commentable_type = $faker->randomElement($commentables);
    $commentable = factory($commentable_type)->create();
    $user_id = \App\Models\User::all()->random()->id;
    return [
        'des'=> $faker->text(200),
        'commentable_id' => $commentable->id,
        'commentable_type' => $commentable_type,
        'user_id' => $user_id,
        'created_by' => $user_id,
        'updated_by' => $user_id,
    ];
});
