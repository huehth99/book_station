<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Book::class, function (Faker $faker) {
    return array(
        'name' => $faker->text(40),
        'type' => $faker->text(20),
        'des' => $faker->text(200),
        'url' => 'introduction.pdf',
        'page' => $faker->numberBetween(100, 900),
        'views' => $faker->numberBetween(0, 1000),
        'created_by' => $faker->numberBetween(0, 10),
        'updated_by' => $faker->numberBetween(0, 10),
    );
});
