<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    $user_id = \App\Models\User::all()->random()->id;
    return [
        'user_id' => $user_id,
        'des' => $faker->text(200),
        'created_by' => $user_id,
        'updated_by' => $user_id,
    ];
});
