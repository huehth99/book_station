<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
//        $this->call(BooksTableSeeder::class);
//        $this->call(FollowsTableSeeder::class);
//        $this->call(ReviewsTableSeeder::class);
//        $this->call(CommentsTableSeeder::class);
//        $this->call(RatesTableSeeder::class);
        $this->call(ModelHasRolesTable::class);
    }

}
