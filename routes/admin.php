<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['role:admin', 'auth']], function(){
    Route::get('', 'HomeController@index')->name('admin.index');
    Route::resource('books', 'BookController', [
        'as' => 'admin',
    ]);
    Route::post('add-list-book', 'BookController@addList')->name('admin.books.import');
    Route::get('add-list-book', 'BookController@showAddList')->name('admin.books.addlist');
});
