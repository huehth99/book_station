<?php

namespace App\Repositories\Book;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\DB;

class BookRepository extends EloquentRepository implements BookRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getModel()
    {
        return \App\Models\Book::class;
    }

    public function getListBook()
    {
        return DB::table('books')
            ->select('books.id as id',
                'books.name as name',
                'books.type as type',
                'books.des as des',
                'books.url as url',
                'books.page as page',
                'books.views as views')
            ->orderBy('created_at', 'DESC')
            ->take(10)
            ->get();
    }
}
