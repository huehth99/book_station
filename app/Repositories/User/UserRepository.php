<?php

namespace App\Repositories\User;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\DB;

class UserRepository extends EloquentRepository implements UserRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function getModel()
    {
        return \App\Models\User::class;
    }

    public function getListUser()
    {
        //
    }

}
