<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\Controller;
use App\Imports\BooksImport;
use App\Repositories\Book\BookRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Http\Requests\BookRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;

class BookController extends Controller
{
    protected $bookRepository;
    protected $userRepository;

    public function __construct(BookRepositoryInterface $bookRepository, UserRepositoryInterface $userRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $books = $this->bookRepository->getListBook();
        return view('admin.books.index', compact('books'));
    }

    public function create()
    {
        return view('admin.books.create');
    }

    public function store(BookRequest $request)
    {
        $file = $request->file('url');
        $url = $file->getClientOriginalName();
        $this->saveFile($file);
        $book = array(
            'name' => $request->get('name'),
            'type' => $request->get('type'),
            'des' => $request->get('des'),
            'url' => $url,
            'page' => $this->getTotalPages($file),
            'views' => 0,
        );
        $this->bookRepository->create($book);

        return redirect()->route('admin.books.index');
    }

    public function show($id)
    {
        try {
            $book = $this->bookRepository->find($id);
        } catch (NotFoundException $exception) {
            throw $exception;
        }
        $created_by = $this->userRepository->getModelById($book->created_by)->name ?? 'unidentified user';
        $updated_by = $this->userRepository->getModelById($book->updated_by)->name ?? 'unidentified user';

        return view('admin.books.detail', [
            'book' => $book,
            'created_by' => $created_by,
            'updated_by' => $updated_by,
        ]);
    }

    public function edit($id)
    {
        try {
            $book = $this->bookRepository->find($id);
        } catch (NotFoundException $exception) {
            throw $exception;
        }

        return view('admin.books.edit', compact('book'));
    }

    public function update(BookRequest $request, $id)
    {
        $book = $this->bookRepository->getModelById($id);
        $attribute = $request->all();
        if ($request->hasFile('url'))
        {
            $file = $request->file('url');
            $url = $file->getClientOriginalName();
            $this->saveFile($file);
            $attribute = array_merge($attribute, array(
                'url' => $url,
                'page' => $this->getTotalPages($file),
            ));
        }
        $book->update($attribute);

        return redirect()->route('admin.books.index');
    }

    public function saveFile(UploadedFile $file)
    {
        $filename = $file->getClientOriginalName();
        $file->storeAs('public/pdf', $filename);
    }

    public function getTotalPages($file)
    {
        $pdftext = file_get_contents($file);
        $page = preg_match_all("/\/Page\W/", $pdftext, $dummy);

        return $page;
    }

    public function destroy($id)
    {
        try {
            $book = $this->bookRepository->find($id);
        } catch (NotFoundException $exception) {
            throw $exception;
        }
        $book->delete($id);

        return response()->json("success", 200);
    }

    public function addList(Request $request)
    {
        $filezip = $request->file('file');
        if ($this->exactFile($filezip))
        {
            $unzipfile = substr($filezip->getClientOriginalName(), 0, -4);
            $path = storage_path('app/public/zip/'.$unzipfile);
            $files = File::allFiles($path);
            $listpdfs = array();
            $filexlsx = new File;
            foreach ($files as $file)
            {
                if ($file->getExtension() == 'pdf')
                {
                    $page = $this->getTotalPages($file);
                    $pdf = array(
                        $file->getFilename() => $page,
                    );
                    $listpdfs = array_merge($listpdfs, $pdf);
                    File::copy($path.'/'.$file->getFilename(), storage_path('app/public/pdf/'.$file->getFilename()));
                } elseif ($file->getExtension() == 'xlsx') {
                    $filexlsx = $file;
                }
            }
            $pathxlsx = $path.'/'.$filexlsx->getFilename();
            $this->import($pathxlsx, $listpdfs);
        } else {
            return Redirect::back()->withErrors(['File open failed!']);
        }

        return redirect()->route('admin.books.index');
    }

    public function import($filepath, $listpdf)
    {
        $import = new BooksImport($listpdf);
        Excel::import($import, $filepath);
    }

    public function exactFile(UploadedFile $file)
    {
        $filename = $file->getClientOriginalName();
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $valid_ext = array('zip');
        if(in_array(strtolower($ext),$valid_ext)){
            $tmp_name = $file->getPathname();
            $zip = new ZipArchive;
            $res = $zip->open($tmp_name);
            if ($res === TRUE) {
                $path = storage_path('app/public/zip');
                $zip->extractTo($path);
                $zip->close();
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public function showAddList(Request $request)
    {
        return view('admin.books.addlist');
    }
}
