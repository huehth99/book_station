<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'books';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'des',
        'url',
        'page',
        'views',
        'created_by',
        'updated_by',
    ];

    public function users() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function rates()
    {
        return $this->morphMany('App\Models\Rate', 'ratable');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
}
