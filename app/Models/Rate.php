<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'rates';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'star',
        'ratable_id',
        'ratable_type',
        'created_by',
        'updated_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ratable()
    {
        return $this->morphTo();
    }
}
