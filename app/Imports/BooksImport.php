<?php

namespace App\Imports;

use App\Models\Book;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BooksImport implements ToCollection
{
    protected $book;
    protected $listpdfs;

    public function __construct(array $listpdfs)
    {
        $this->book = new Book;
        $this->listpdfs = $listpdfs;
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            $this->book->create([
                'name' => $row[0],
                'type' => $row[1],
                'des' => $row[2],
                'url' => $row[3],
                'page' => $this->listpdfs[$row[3]],
                'views' => 0,
            ]);
        }
    }
}
