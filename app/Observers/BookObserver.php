<?php

namespace App\Observers;

use App\Models\Book;
use Illuminate\Support\Facades\Auth;

class BookObserver
{

    public function creating(Book $book)
    {
        $book->created_by = Auth::user()->id;
        $book->updated_by = Auth::user()->id;
    }

    public function updating(Book $book)
    {
        $book->updated_by = Auth::user()->id;
    }
}
